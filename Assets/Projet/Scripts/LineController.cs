using UnityEngine;

public class LineController : MonoBehaviour
{

    private LineRenderer lineRenderer;
    [SerializeField] private Transform[] pointTransforms;

    private void Start()
    {
        // Cr�er un nouvel objet LineRenderer
        lineRenderer = GetComponent<LineRenderer>();
        
    }

    private void Update()
    {
        lineRenderer.positionCount = pointTransforms.Length;
        for (int i = 0; i < pointTransforms.Length; i++)
        {
            lineRenderer.SetPosition(i, pointTransforms[i].position);
        }
    }
}

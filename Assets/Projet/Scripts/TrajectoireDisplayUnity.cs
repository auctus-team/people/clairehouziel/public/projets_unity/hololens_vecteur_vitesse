using System.Collections.Generic;
using UnityEngine;

public class TrajectoireDisplayUnity : MonoBehaviour
{
    public GameObject robotEffector; // R�f�rence � l'ArticulationBody de l'effecteur du robot
    public GameObject arrowObject; // GameObject en forme de fl�che � placer

    private void Start()
    {
        if (robotEffector == null || arrowObject == null)
        {
            Debug.LogError("R�f�rences manquantes. Assurez-vous d'assigner les objets dans l'inspecteur.");
        }
    }

    private void Update()
    {
        if (robotEffector == null || arrowObject == null)
        {
            return;
        }

        // R�cup�rer la pose de l'effecteur du robot
        Vector3 effectorPosition = robotEffector.transform.position;
        Quaternion effectorOrientation = robotEffector.transform.rotation;

        // D�finir la pose de la fl�che en fonction de l'effecteur
        transform.rotation = effectorOrientation;
        transform.position = effectorPosition ;

        // Ajouter une rotation de -45 degr�s autour de l'axe y local de la fl�che (pour l'orienter comme une fl�che)
        //transform.Rotate(Vector3.up, -45f, Space.Self);




    }

}

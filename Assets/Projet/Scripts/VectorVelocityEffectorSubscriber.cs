using Unity.Robotics.ROSTCPConnector;
using RosMessageTypes.Geometry;
using UnityEngine;

public class VectorVelocityEffectorSubscriber : MonoBehaviour
{
    //topic ROS �cout�
    [SerializeField]
    string topicName = "/panda/velocity_effector";

    // Connecteur ROS
    private ROSConnection rosConnection;

    // Singleton instance
    private static VectorVelocityEffectorSubscriber instance;

    // Get the singleton instance of this script
    public static VectorVelocityEffectorSubscriber Instance
    {
        get { return instance; }
    }

    // variables o� on stocke les derni�res valeurs publi�es 
    private Vector3 linearVelocity;
    private Vector3 angularVelocity;



    // Start is called before the first frame update
    void Start()
    {

        // Initialisation de l'instance de ce script
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
            return;
        }

        DontDestroyOnLoad(gameObject);

        // Connexion � ROS (n�cessite type de message et nom d'un "callback" : ici ReceiveVector) 
        rosConnection = ROSConnection.GetOrCreateInstance();
        rosConnection.Subscribe<TwistMsg>(topicName, ReceiveVector);

    }

    private void ReceiveVector(TwistMsg twistMsg)
    {
        // on actualise les valeurs de vitesses lin�aires et angulaires
        // on convertit les coordoonn�es du syst�me right-handed du robot au left-handed du QR Code d'Unity
        linearVelocity = new Vector3((float) - twistMsg.linear.y, (float) twistMsg.linear.z, (float) twistMsg.linear.x);
        angularVelocity = new Vector3((float) twistMsg.angular.x, (float) twistMsg.angular.z, (float) twistMsg.angular.y);

        Debug.Log(linearVelocity);

    }

    //M�thodes publiques pour acc�der aux derni�res donn�es � partir d'un autre script
    public Vector3 LinearVelocity
    {
        get { return linearVelocity; }
    }

    public Vector3 AngularVelocity
    {
        get { return angularVelocity; }
    }
}
